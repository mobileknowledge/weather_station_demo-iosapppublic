//
//  WeatherStationDemoTests.swift
//  WeatherStationDemoTests
//
//  Created by MKMac1 on 27/09/2018.
//  Copyright © 2018 MK. All rights reserved.
//

import XCTest
@testable import WeatherStationDemo

class WeatherStationDemoTests: XCTestCase {

    var deviceUnderTest: Device!
    
    override func setUp() {
        super.setUp()
        deviceUnderTest = Device.deviceInstance
        deviceUnderTest.setName(name: "TestDevice")
        deviceUnderTest.setBleAddress(address: "0123:4567:89AB:CDEF")
    }

    override func tearDown() {
    }

    func testAddFirstTemperatureWhenArrayIsZero() {
        //Given
        let measure = Measurement(value: 25.00, timestamp: nil)
        
        //When
        deviceUnderTest.addTemperatureValue(measure: measure)
        
        //Then
        XCTAssertEqual(deviceUnderTest.getTemperature(position: 0).value, measure.value, "The temperature is not correctly introduced in the array")
    }

    func testAddTemperatureWhenArrayIsFull() {
        //Given
        let temperatures: [Float] = [30.0, 31.0, 32.0, 33.0]
        let newMeasure = Measurement(value: 35.3, timestamp: nil)
        for temp in temperatures {
            deviceUnderTest.addTemperatureValue(measure: Measurement(value: temp, timestamp: nil))
        }
        
        //When
        deviceUnderTest.addTemperatureValue(measure: newMeasure)
        
        //Then
        XCTAssertEqual(deviceUnderTest.getTemperature(position: 0).value, newMeasure.value, "The temperature is not correctly introduced in the array")
        XCTAssertNotEqual(deviceUnderTest.getTemperature(position: 1).value, newMeasure.value, "The temperature is not correctly introduced in the array")
        XCTAssertNotEqual(deviceUnderTest.getTemperature(position: 2).value, newMeasure.value, "The temperature is not correctly introduced in the array")
        XCTAssertNotEqual(deviceUnderTest.getTemperature(position: 3).value, newMeasure.value, "The temperature is not correctly introduced in the array")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
