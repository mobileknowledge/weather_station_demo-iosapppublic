//
//  Persistent.swift
//  WeatherStationDemo
//
//  Created by MKMac1 on 27/09/2018.
//  Copyright © 2018 MK. All rights reserved.
//

import Foundation

class Persistent : NSObject{
    //Read the persistent memory (Preferences) to get the value of the cloud post setting
    static func readCloudPostSettings() -> Bool?{
        let preferences = UserDefaults.standard
        var currentSettings : Bool?
        
        let cloudSettingsKey = "CloudSettings"
        if preferences.object(forKey: cloudSettingsKey) == nil{
            print("Cloud settigns doesn't exist in persistent memory")
            return currentSettings
        }
        else{
            currentSettings = preferences.bool(forKey: cloudSettingsKey)
            return currentSettings
        }
    }
    
    //Write the persistent memory (Preferences) to set the value of the cloud post setting
    static func writeCloudPostSettings(enable: Bool) -> Bool{
        let preferences = UserDefaults.standard
        
        let currentSettings = enable
        let cloudSettingsKey = "CloudSettings"
        preferences.set(currentSettings, forKey: cloudSettingsKey)
        
        return preferences.synchronize()
    }
    
    //Read the persistent memory (Preferences) to get the value of the session ID
    static func readSessionID() -> String?{
        let preferences = UserDefaults.standard
        var currentSessionID : String?
        
        let sessionIDKey = "SessionID"
        if preferences.object(forKey: sessionIDKey) == nil{
            print("SessionID doesn't exist in persistent memory")
            return currentSessionID
        }
        else{
            currentSessionID = preferences.string(forKey: sessionIDKey)
            return currentSessionID
        }
    }
    
    //Write the persistent memory (Preferences) to set the value of the session ID
    static func writeSessionID(sessionID: String?) -> Bool{
        let preferences = UserDefaults.standard
        
        let currentSessionID = sessionID
        let sessionIDKey = "SessionID"
        preferences.set(currentSessionID, forKey: sessionIDKey)
        
        return preferences.synchronize()
    }
    
    //Read the persistent memory (Preferences) to get the value of the data refresh setting
    static func readDataRefreshRate() -> Int?{
        let preferences = UserDefaults.standard
        var currentSettings : Int?
        
        let dataRefreshKey = "DataRefresh"
        if preferences.object(forKey: dataRefreshKey) == nil{
            print("Data refresh rate doesn't exist in persistent memory")
            return currentSettings
        }
        else{
            currentSettings = preferences.integer(forKey: dataRefreshKey)
            return currentSettings
        }
    }
    
    //Write the persistent memory (Preferences) to set the value of the data refresh setting
    static func writeDataRefreshRate(rateInSeconds: Int) -> Bool{
        let preferences = UserDefaults.standard
        
        let currentSettings = rateInSeconds
        let dataRefreshKey = "DataRefresh"
        preferences.set(currentSettings, forKey: dataRefreshKey)
        
        return preferences.synchronize()
    }
    
    //Read the persistent memory (Preferences) to get the value of the temperature unit setting
    static func readTemperatureUnit() -> String?{
        let preferences = UserDefaults.standard
        var currentSettings : String?
        
        let tempUnitKey = "TempUnit"
        if preferences.string(forKey: tempUnitKey) == nil{
            print("Temperature unit doesn't exist in persistent memory")
            return currentSettings
        }
        else{
            currentSettings = preferences.string(forKey: tempUnitKey)
            return currentSettings
        }
    }
    
    //Write the persistent memory (Preferences) to set the value of the temperature unit setting
    static func writeTemperatureUnit(unit: String) -> Bool{
        let preferences = UserDefaults.standard
        
        let currentSettings = unit
        let tempUnitKey = "TempUnit"
        preferences.set(currentSettings, forKey: tempUnitKey)
        
        return preferences.synchronize()
    }
}
