//
//  WelcomeScreenController.swift
//  WeatherStationDemo
//
//  Created by MKMac1 on 27/09/2018.
//  Copyright © 2018 MK. All rights reserved.
//

import Foundation
import UIKit
import CoreBluetooth

//Welcome view controller contains the table with the available Rapid IoT devices to connect, it has the access for the Settings and Tutorial
class WelcomeViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, BLEDelegate {
    
    @IBOutlet weak var devicesTable: UITableView! //Instance to the table that shows the scanned devices
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var tutorialButton: UIButton!
    
    @IBOutlet weak var connectionLabel: UILabel!
    
    var bleDevicesList: [(peripheral: CBPeripheral, advName: String, lastUpdate: Date)] = []
    
    var selectedIndexPath: IndexPath!
    var network: Network? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        settingsButton.layer.cornerRadius = 15
        tutorialButton.layer.cornerRadius = 15
        
        //Set default values for data refresh rate, temperature unit and cloud post settings
        if(Persistent.readDataRefreshRate() == nil){
            if(!Persistent.writeDataRefreshRate(rateInSeconds: 1)){
                print("Error storing data in persistent memory")
            }
        }
        if(Persistent.readTemperatureUnit() == nil){
            if(!Persistent.writeTemperatureUnit(unit: Constants.TemperatureUnit.celsius.rawValue)){
                print("Error storing data in persistent memory")
            }
        }
        if(Persistent.readCloudPostSettings() == nil){
            if(!Persistent.writeCloudPostSettings(enable: true)){
                print("Error storing data in persistent memory")
            }
        }
        
        //Check if the mobile phone has internet capabilities activated.
        if(!Network.checkNetworkConnection()){
            let alert = UIAlertController(title: "Internet error", message: "There is no internet connection available, please it turn in order to be able to upload the data to the cloud application.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: openSettings))
            self.present(alert, animated: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        devicesTable.delegate = self
        devicesTable.dataSource = self

        BLEManager.sharedBLEInstance.delegate = self

        if(BLEManager.sharedBLEInstance.isConnected()){
            let deviceName = BLEManager.sharedBLEInstance.getCurrentPeripheralName()
            connectionLabel.text = "Connected to: \(deviceName ?? "Unknown")"
        }
        else{
            connectionLabel.text = "No device connected"
            self.devicesTable.reloadData()
        }
        
        //In case the BLE adapter is turned on and the bluetooth scan has not started yet, start discovering devices
        if(BLEManager.sharedBLEInstance.getBLEState() == CBManagerState.poweredOn){
            if(!BLEManager.sharedBLEInstance.isBLEScanning())
            {
                print("Bluetooth scan started")
                BLEManager.sharedBLEInstance.startScan()
            }
        }
        
        //Set a refresh time for the devices table each 7 seconds.
        Timer.scheduledTimer(withTimeInterval: 7, repeats: true){ timer in
            DispatchQueue.main.async {
                if(BLEManager.sharedBLEInstance.isBLEScanning()){
                    for i in (0..<self.bleDevicesList.count).reversed(){
                        if ((self.bleDevicesList[i].lastUpdate.timeIntervalSinceNow < -2.0) && (self.bleDevicesList[i].peripheral != BLEManager.sharedBLEInstance.getCurrentPeripheral())) {
                            self.bleDevicesList.remove(at: i)
                            self.devicesTable.reloadData()
                        }
                    }
                }
            }
        }
        
        //Setup for the long press gesture recognizer, this function is prompted when the user performs a long click in any device in the list
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.longPress(press:)))
        longPressRecognizer.minimumPressDuration = 1.0
        self.devicesTable.addGestureRecognizer(longPressRecognizer)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        BLEManager.sharedBLEInstance.stopScan()
    }
    
    @objc func longPress(press: UILongPressGestureRecognizer){
        //Detect the cell the user is tapping and check if the device tapped is the same the user is currently connected to
        if(press.state == UIGestureRecognizer.State.began){
            let touchPoint = press.location(in: self.devicesTable)
            if let indexPath = self.devicesTable.indexPathForRow(at: touchPoint){
                if ((press.state == UIGestureRecognizer.State.began) && (bleDevicesList[indexPath.row].advName == BLEManager.sharedBLEInstance.getCurrentPeripheralName())){
                    showDisconnectLongPressDialog()
                }
            }
        }
    }
    
    //Open the devices settings to activate Bluetooth
    func openSettings(alert: UIAlertAction!){
        UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
    }
    
    func showDisconnectDialog(indexPath: IndexPath){
        let uiAlert = UIAlertController(title: "Disconnect device", message: "Please confirm you want to disconnect from the device and connect to a new one", preferredStyle: UIAlertController.Style.alert)
        
        uiAlert.addAction(UIAlertAction(title: "Yes", style: .cancel, handler: {action in
            if(BLEManager.sharedBLEInstance.disconnectFromPeripheral())
            {
                BLEManager.sharedBLEInstance.stopScan()
                BLEManager.sharedBLEInstance.connectToPeripheral(peripheral: self.bleDevicesList[indexPath.row].peripheral)
                self.selectedIndexPath = indexPath
            }
            else{
                print("Couldn't disconnect from the device")
            }
            
        }))
        
        uiAlert.addAction(UIAlertAction(title: "No", style: .default, handler: nil))

        self.present(uiAlert, animated: true, completion: nil)
    }
    
    func showDisconnectLongPressDialog(){
        let uiAlert = UIAlertController(title: "Disconnect device", message: "Please confirm you want to disconnect from the device", preferredStyle: UIAlertController.Style.alert)
        
        uiAlert.addAction(UIAlertAction(title: "Yes", style: .cancel, handler: {action in
            if(!BLEManager.sharedBLEInstance.disconnectFromPeripheral())
            {
                print("Couldn't disconnect from the device")
            }
        }))
        
        uiAlert.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
        
        self.present(uiAlert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bleDevicesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = devicesTable.dequeueReusableCell(withIdentifier: "cell") as! ScanDevicesTableViewCell
        
        //Take the device name from the devices list and check if it matches with a device currently connected to the app, in that case draw the cell in a different color.
        cell.deviceLabel.text = bleDevicesList[indexPath.row].advName
        if(BLEManager.sharedBLEInstance.isConnected() && (bleDevicesList[indexPath.row].advName == BLEManager.sharedBLEInstance.getCurrentPeripheralName())){
            cell.backgroundColor = UIColor(red: 0.68, green: 0.74, blue: 1.00, alpha: 1.0)
        }
        else{
            cell.backgroundColor = UIColor.clear
        }
                
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.devicesTable.deselectRow(at: indexPath, animated: true)
        
        //When the user taps a cell there are three different options:
        // 1 - The device tapped is not the same device it is connected to the app, in this case show a dialog to warn the user if he wants to switch devices
        // 2 - The device tapped is the same device connected to the app, take the user to the Data display view
        // 3 - There are no devices connected to the app, in this case stops the bluetooth scanning and begins the connection procedure with the selected device
        if(BLEManager.sharedBLEInstance.isConnected() && (bleDevicesList[indexPath.row].advName != BLEManager.sharedBLEInstance.getCurrentPeripheralName())){
            showDisconnectDialog(indexPath: indexPath)
        }
        else if (BLEManager.sharedBLEInstance.isConnected() && (bleDevicesList[indexPath.row].advName == BLEManager.sharedBLEInstance.getCurrentPeripheralName())){
            self.performSegue(withIdentifier: "ShowView", sender: self)
        }
        else{
            BLEManager.sharedBLEInstance.stopScan()
            BLEManager.sharedBLEInstance.connectToPeripheral(peripheral: bleDevicesList[indexPath.row].peripheral)
            selectedIndexPath = indexPath
        }
    }
    
    //BLEDelegate implementations
    func bleDidUpdateState(state: CBManagerState) {
        BLEManager.sharedBLEInstance.setBLEState(mState: state)
        
        if(state == .poweredOn){
            if(!BLEManager.sharedBLEInstance.isBLEScanning())
            {
                print("Bluetooth scan started")
                BLEManager.sharedBLEInstance.startScan()
            }
        }
        else if(state == .poweredOff){
            if(BLEManager.sharedBLEInstance.isBLEScanning()){
                print("Bluetooth scan stopped")
                BLEManager.sharedBLEInstance.stopScan()
            }
            
            let alert = UIAlertController(title: "Bluetooth error", message: "The bluetooth adapter is not connected, please turn on the Bluetooth on the device to start the devices scan", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: openSettings))
            self.present(alert, animated: true)
        }
        else if(state == .unsupported){
            if(BLEManager.sharedBLEInstance.isBLEScanning()){
                print("Bluetooth scan stopped")
                BLEManager.sharedBLEInstance.stopScan()
            }
            
            let alert = UIAlertController(title: "Bluetooth error", message: "The bluetooth adapter is unsupported in your mobile phone, unfortunately the application will not be able to connect to a Rapid IoT device in this phone", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: openSettings))
            self.present(alert, animated: true)
        }
        else{
            if(BLEManager.sharedBLEInstance.isBLEScanning()){
                print("Bluetooth scan stopped")
                BLEManager.sharedBLEInstance.stopScan()
            }
            
            let alert = UIAlertController(title: "Bluetooth error", message: "The bluetooth adapter is in unknown state, try to reset it and enter in the application again", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: openSettings))
            self.present(alert, animated: true)
        }
    }
    
    func bleDidConnectToPeripheral(peripheral: CBPeripheral?) {
        let peripheralName = bleDevicesList[selectedIndexPath.row].advName
        BLEManager.sharedBLEInstance.setCurrentPeripheralName(advName: bleDevicesList[selectedIndexPath.row].advName)
        connectionLabel.text = "Connected to: \(peripheralName)"
        
        Device.deviceInstance.setName(name: peripheralName)
        
        //Read MAC Address charactersitic
        BLEManager.sharedBLEInstance.readMACAddressCharacteristic()
        
        //Generate random number to log into the cloud application
        let mRandomNumber = RNG.generateRandomNumber(digits: 6)
        if(!Persistent.writeSessionID(sessionID: mRandomNumber)){
            print("Could not write the session ID in the persistent memory")
        }
        
        self.devicesTable.reloadData()
        self.performSegue(withIdentifier: "ShowView", sender: self)
    }
    
    func bleDidDisconnectFromPeripheral() {
        connectionLabel.text = "No device connected"
        
        network?.disconnectFromBroker()
        
        BLEManager.sharedBLEInstance.startScan()
        
        //Clear sessionID from persistent memory
        if(!Persistent.writeSessionID(sessionID: nil)){
            print("Could not write the session ID in the persistent memory")
        }
        
        self.devicesTable.reloadData()
    }
    
    func bleDidReceiveData(char: String, data: Data?) {
        print("Received data from the  \(char), with data: \(String(describing: data))")
    }
    
    func bleDidDiscoverDevice(peripheral: CBPeripheral?, advertisementLocalName: String?){
        if let mPeripheral = peripheral?.name{
            print("Discovered device: " + mPeripheral)
        }
        else {
            print("Couldnt get name of discovered device")
        }
        
        //Check if the device name is not null and if there is any device with the same name already recorded in the devices list.
        if(peripheral?.name != nil && advertisementLocalName != nil && !(bleDevicesList.map{$0.0}).contains(peripheral!)){
            bleDevicesList.append((peripheral!, advertisementLocalName!, Date()))
        }
        else if let i = bleDevicesList.index(where: ({$0.peripheral === peripheral})){
            bleDevicesList[i].lastUpdate = Date()
        }
        
        //Update devices table to show the user the new discovered device (if any)
        devicesTable.reloadData()
    }
    
    func bleDidFailConnectToPeripheral() {
        print("Failed to connect to peripheral")
    }
    
}
