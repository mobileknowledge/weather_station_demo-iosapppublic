//
//  LightViewController.swift
//  WeatherStationDemo
//
//  Created by MKMac1 on 22/10/2018.
//  Copyright © 2018 MK. All rights reserved.
//

import UIKit

//Tab controller for the Light measurements. This class implement the UpdateTableProtocol that notifies when the table needs to be updated.
class LightTabController: UIViewController, UpdateTableProtocol  {
    
    @IBOutlet weak var lightLabel_1: UILabel!
    
    @IBOutlet weak var timestampLabel_1: UILabel!
    
    @IBOutlet weak var lightLabel_2: UILabel!
    
    @IBOutlet weak var timestampLabel_2: UILabel!
    
    @IBOutlet weak var lightLabel_3: UILabel!
    
    @IBOutlet weak var timestampLabel_3: UILabel!
    
    @IBOutlet weak var lightLabel_4: UILabel!
    
    @IBOutlet weak var timestampLabel_4: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set the delegate to this class
        TableUpdater.tableUpdaterInstance.updateLightTableDelegate = self
        self.updateTable()
    }
    
    func updateTable(sensorType: Constants.SensorType) {
        if(sensorType == Constants.SensorType.light){
            self.updateTable()
        }
    }
    
    //Each time the table is updated the last value retrieved from the sensors is added at the top of the table and the first value is shown at the bottom.
    func updateTable(){
        let formatter = DateFormatter()
        
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "HH:mm:ss"
        
        if let light_0 = Device.deviceInstance.getLight(position:0).value{
            lightLabel_1.text = "\(light_0) lux"
            
            let formattedDate = formatter.string(from: Date(timeIntervalSince1970: Double((Device.deviceInstance.getLight(position: 0).timestamp!))))
            timestampLabel_1.text = formattedDate
        }
        
        if let light_1 = Device.deviceInstance.getLight(position:1).value{
            lightLabel_2.text = "\(light_1) lux"
            
            let formattedDate = formatter.string(from: Date(timeIntervalSince1970: Double((Device.deviceInstance.getLight(position: 1).timestamp!))))
            timestampLabel_2.text = formattedDate
        }
        
        if let light_2 = Device.deviceInstance.getLight(position:2).value{
            lightLabel_3.text = "\(light_2) lux"
            
            let formattedDate = formatter.string(from: Date(timeIntervalSince1970: Double((Device.deviceInstance.getLight(position: 2).timestamp!))))
            timestampLabel_3.text = formattedDate
        }
        
        if let light_3 = Device.deviceInstance.getLight(position:3).value{
            lightLabel_4.text = "\(light_3) lux"
            
            let formattedDate = formatter.string(from: Date(timeIntervalSince1970: Double((Device.deviceInstance.getLight(position: 3).timestamp!))))
            timestampLabel_4.text = formattedDate
        }
    }
    
}
