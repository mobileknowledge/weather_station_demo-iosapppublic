//
//  MeasurementsTableViewController.swift
//  WeatherStationDemo
//
//  Created by MKMac1 on 04/10/2018.
//  Copyright © 2018 MK. All rights reserved.
//

import UIKit
import CoreBluetooth

//Tab bar controller, this view will only contain the Tab Bar controller.
class MeasurementsTabBarController: UITabBarController {
  
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
}
