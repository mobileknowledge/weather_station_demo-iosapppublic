//
//  PressureViewController.swift
//  WeatherStationDemo
//
//  Created by MKMac1 on 22/10/2018.
//  Copyright © 2018 MK. All rights reserved.
//

import UIKit

//Tab controller for the Pressure measurements. This class implement the UpdateTableProtocol that notifies when the table needs to be updated.
class PressureTabController: UIViewController, UpdateTableProtocol  {

    @IBOutlet weak var pressLabel_1: UILabel!
    @IBOutlet weak var timestampLabel_1: UILabel!
    
    @IBOutlet weak var pressLabel_2: UILabel!
    
    @IBOutlet weak var timestampLabel_2: UILabel!
    
    @IBOutlet weak var pressLabel_3: UILabel!
    
    @IBOutlet weak var timestampLabel_3: UILabel!
    
    @IBOutlet weak var pressLabel_4: UILabel!
    
    @IBOutlet weak var timestampLabel_4: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        //Set the delegate to this class
        TableUpdater.tableUpdaterInstance.updatePressureTableDelegate = self
        self.updateTable()
    }
    
    func updateTable(sensorType: Constants.SensorType) {
        if(sensorType == Constants.SensorType.pressure){
            self.updateTable()
        }
    }
    
    //Each time the table is updated the last value retrieved from the sensors is added at the top of the table and the first value is shown at the bottom.
    func updateTable(){
        let formatter = DateFormatter()
        
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "HH:mm:ss"
        
        if let pressure_0 = Device.deviceInstance.getPressure(position:0).value{
            pressLabel_1.text = "\(pressure_0) hPa"
            
            let formattedDate = formatter.string(from: Date(timeIntervalSince1970: Double((Device.deviceInstance.getPressure(position: 0).timestamp!))))
            timestampLabel_1.text = formattedDate
        }
        
        if let pressure_1 = Device.deviceInstance.getPressure(position:1).value{
            pressLabel_2.text = "\(pressure_1) hPa"
            
            let formattedDate = formatter.string(from: Date(timeIntervalSince1970: Double((Device.deviceInstance.getPressure(position: 1).timestamp!))))
            timestampLabel_2.text = formattedDate
        }
        
        if let pressure_2 = Device.deviceInstance.getPressure(position:2).value{
            pressLabel_3.text = "\(pressure_2) hPa"
            
            let formattedDate = formatter.string(from: Date(timeIntervalSince1970: Double((Device.deviceInstance.getPressure(position: 2).timestamp!))))
            timestampLabel_3.text = formattedDate
        }
        
        if let pressure_3 = Device.deviceInstance.getPressure(position:3).value{
            pressLabel_4.text = "\(pressure_3) hPa"
            
            let formattedDate = formatter.string(from: Date(timeIntervalSince1970: Double((Device.deviceInstance.getPressure(position: 3).timestamp!))))
            timestampLabel_4.text = formattedDate
            
        }
    }
    
}
