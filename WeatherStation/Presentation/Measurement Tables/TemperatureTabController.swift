//
//  File.swift
//  WeatherStationDemo
//
//  Created by Luis Pascual on 16/10/2018.
//  Copyright © 2018 MK. All rights reserved.
//

import Foundation
import UIKit

//Tab controller for the Temperature measurements. This class implement the UpdateTableProtocol that notifies when the table needs to be updated.
class TemperatureTabController: UIViewController, UpdateTableProtocol {
    
    @IBOutlet weak var tempLabel_1: UILabel!
    
    @IBOutlet weak var timestampLabel_1: UILabel!
    
    @IBOutlet weak var tempLabel_2: UILabel!
    
    @IBOutlet weak var timestampLabel_2: UILabel!
    
    @IBOutlet weak var tempLabel_3: UILabel!
    
    @IBOutlet weak var timestampLabel_3: UILabel!
    
    @IBOutlet weak var tempLabel_4: UILabel!
    
    @IBOutlet weak var timestampLabel_4: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set the delegate to this class
        TableUpdater.tableUpdaterInstance.updateTempTableDelegate = self
        self.updateTable()
    }
    
    func updateTable(sensorType: Constants.SensorType) {
        if(sensorType == Constants.SensorType.temperature){
            self.updateTable()
        }
    }
    
    //Each time the table is updated the last value retrieved from the sensors is added at the top of the table and the first value is shown at the bottom.
    func updateTable(){
        let formatter = DateFormatter()
        
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "HH:mm:ss"
        
        if let temp_0 = Device.deviceInstance.getTemperature(position:0).value{
            if(Persistent.readTemperatureUnit() == Constants.TemperatureUnit.celsius.rawValue){
                tempLabel_1.text = "\(temp_0) ºC"
            }
            else{
                let fahrenheitValue = DataFormatting.convertCelsiusToFahrenheit(value: temp_0)
                tempLabel_1.text = "\(String(format: "%.2f", fahrenheitValue))ºF"
            }
            
            let formattedDate = formatter.string(from: Date(timeIntervalSince1970: Double((Device.deviceInstance.getTemperature(position: 0).timestamp!))))
            timestampLabel_1.text = formattedDate
        }
        
        if let temp_1 = Device.deviceInstance.getTemperature(position:1).value{
            if(Persistent.readTemperatureUnit() == Constants.TemperatureUnit.celsius.rawValue){
                tempLabel_2.text = "\(temp_1) ºC"
            }
            else{
                let fahrenheitValue = DataFormatting.convertCelsiusToFahrenheit(value: temp_1)
                tempLabel_2.text = "\(String(format: "%.2f", fahrenheitValue))ºF"
            }
            
            let formattedDate = formatter.string(from: Date(timeIntervalSince1970: Double((Device.deviceInstance.getTemperature(position: 1).timestamp!))))
            timestampLabel_2.text = formattedDate
        }
        
        if let temp_2 = Device.deviceInstance.getTemperature(position:2).value{
            if(Persistent.readTemperatureUnit() == Constants.TemperatureUnit.celsius.rawValue){
                tempLabel_3.text = "\(temp_2) ºC"
            }
            else{
                let fahrenheitValue = DataFormatting.convertCelsiusToFahrenheit(value: temp_2)
                tempLabel_3.text = "\(String(format: "%.2f", fahrenheitValue))ºF"
            }
            
            let formattedDate = formatter.string(from: Date(timeIntervalSince1970: Double((Device.deviceInstance.getTemperature(position: 2).timestamp!))))
            timestampLabel_3.text = formattedDate
        }
        
        if let temp_3 = Device.deviceInstance.getTemperature(position:3).value{
            if(Persistent.readTemperatureUnit() == Constants.TemperatureUnit.celsius.rawValue){
                tempLabel_4.text = "\(temp_3) ºC"
            }
            else{
                let fahrenheitValue = DataFormatting.convertCelsiusToFahrenheit(value: temp_3)
                tempLabel_4.text = "\(String(format: "%.2f", fahrenheitValue))ºF"
            }
            
            let formattedDate = formatter.string(from: Date(timeIntervalSince1970: Double((Device.deviceInstance.getTemperature(position: 3).timestamp!))))
            timestampLabel_4.text = formattedDate
            
        }
    }
    
}
