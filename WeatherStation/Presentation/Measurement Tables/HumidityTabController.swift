//
//  HumidityViewController.swift
//  WeatherStationDemo
//
//  Created by MKMac1 on 21/10/2018.
//  Copyright © 2018 MK. All rights reserved.
//

import Foundation
import UIKit

//Tab controller for the Humidity measurements. This class implement the UpdateTableProtocol that notifies when the table needs to be updated.
class HumidityTabController: UIViewController, UpdateTableProtocol  {
 
    @IBOutlet weak var humidityLabel_1: UILabel!
   
    @IBOutlet weak var timestampLabel_1: UILabel!
    
    @IBOutlet weak var humidityLabel_2: UILabel!
    @IBOutlet weak var timestampLabel_2: UILabel!
    
    @IBOutlet weak var humidityLabel_3: UILabel!
    @IBOutlet weak var timestampLabel_3: UILabel!
    
    @IBOutlet weak var humidityLabel_4: UILabel!
    
    @IBOutlet weak var timestampLabel_4: UILabel!
    
    // MARK: UITableViewDataSource
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set the delegate to this class
        TableUpdater.tableUpdaterInstance.updateHumidityTableDelegate = self
        self.updateTable()
    }
    
    func updateTable(sensorType: Constants.SensorType) {
        if(sensorType == Constants.SensorType.humidity){
            self.updateTable()
        }
    }
    
    //Each time the table is updated the last value retrieved from the sensors is added at the top of the table and the first value is shown at the bottom.
    func updateTable(){
        let formatter = DateFormatter()
        
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "HH:mm:ss"
        
        if let humidity_0 = Device.deviceInstance.getHumidity(position:0).value{
            humidityLabel_1.text = "\(humidity_0) %"
            
            let formattedDate = formatter.string(from: Date(timeIntervalSince1970: Double((Device.deviceInstance.getHumidity(position: 0).timestamp!))))
            timestampLabel_1.text = formattedDate
            
        }
        
        if let humidity_1 = Device.deviceInstance.getHumidity(position:1).value{
            humidityLabel_2.text = "\(humidity_1) %"
            
            let formattedDate = formatter.string(from: Date(timeIntervalSince1970: Double((Device.deviceInstance.getHumidity(position: 1).timestamp!))))
            timestampLabel_2.text = formattedDate
        }
        
        if let humidity_2 = Device.deviceInstance.getHumidity(position:2).value{
            humidityLabel_3.text = "\(humidity_2) %"
            
            let formattedDate = formatter.string(from: Date(timeIntervalSince1970: Double((Device.deviceInstance.getHumidity(position: 2).timestamp!))))
            timestampLabel_3.text = formattedDate
        }
        
        if let humidity_3 = Device.deviceInstance.getHumidity(position:3).value{
            humidityLabel_4.text = "\(humidity_3) %"
            
            let formattedDate = formatter.string(from: Date(timeIntervalSince1970: Double((Device.deviceInstance.getHumidity(position: 3).timestamp!))))
            timestampLabel_4.text = formattedDate

        }
    }
    
}
