//
//  CloudSettingsViewController.swift
//  WeatherStationDemo
//
//  Created by MKMac1 on 03/10/2018.
//  Copyright © 2018 MK. All rights reserved.
//

import UIKit

//Protocol method to notify the data display view when the Clear session button has been pressed.
protocol CloudSettingsViewDelegate: class {
    func clearDatabaseTapped(clearDatabase: Bool)
}

//The Cloud settings has two main functionalities: Activate/deactivate cloud posting of the sensor data and clear the database for the current session
class CloudSettingsViewController: UIViewController{
    @IBOutlet weak var postCloudSwitch: UISwitch!
    @IBOutlet weak var clearCloudButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var alertView: UIView!
    
    var cloudSettingsDelegate: CloudSettingsViewDelegate?
    let alertViewGrayColor = UIColor(red: 224.0/255.0, green: 224.0/255.0, blue: 224.0/255.0, alpha: 1)
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupView()
        animateView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        view.layoutIfNeeded()
    }
    
    //Setup the status of the switch in the view according to a read of the corresponding value in the persistent memory.
    func setupView() {
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        
        let cloudPostSettings  = Persistent.readCloudPostSettings()
        if(cloudPostSettings == true){
            postCloudSwitch.setOn(true, animated: true)
        }
        else{
            postCloudSwitch.setOn(false, animated: true)
        }
    }
    
    //Set the frame in which the view will be contained, to make a similar effect as a Dialog for the Android application
    func animateView() {
        alertView.alpha = 0;
        self.alertView.frame.origin.y = self.alertView.frame.origin.y + 50
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.alertView.alpha = 1.0;
            self.alertView.frame.origin.y = self.alertView.frame.origin.y - 50
        })
    }
 
    //Depending on the status of the switch, enable or disable the data in the persistent memory
    @IBAction func postCloudSwitchChange(_ sender: UISwitch) {
        if(sender.isOn){
            if(!Persistent.writeCloudPostSettings(enable: true)){
                print("Could not write in persistent memory")
            }
        }
        else{
            if(!Persistent.writeCloudPostSettings(enable: false)){
                print("Could not write in persistent memory")
            }
        }
    }
    
    //When the user clicks on Clear data he is first warned if he is sure to delete the data, if the user confirms then a clear cloud command is sent to the server.
    @IBAction func clearPostDataButton(_ sender: Any) {
        let uiAlert = UIAlertController(title: "Clear cloud data", message: "Are you sure you want to delete the data related to this device in the database?", preferredStyle: UIAlertController.Style.alert)
        
        uiAlert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: {action in
            self.cloudSettingsDelegate?.clearDatabaseTapped(clearDatabase: true)
        }))
        
        uiAlert.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
        
        self.present(uiAlert, animated: true, completion: nil)
    }
    
    //When clicking "OK" just dismiss the dialog
    @IBAction func submitButtonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
