//
//  TutorialScreenController.swift
//  WeatherStationDemo
//
//  Created by MKMac1 on 27/09/2018.
//  Copyright © 2018 MK. All rights reserved.
//

import Foundation
import UIKit

//TutorialViewController is the global class that will contain the other tutorial view controllers. All these subview controllers are ple-loaded in an array and are displayed in a carousel when this view is loaded.
class TutorialViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    lazy var subViewControllers: [UIViewController] = {
        return [
            UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TutorialConnectViewController") as! TutorialConnectViewController,
            UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TutorialDataTableViewController") as! TutorialDataTableViewController,
            UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TutorialCloudViewController") as! TutorialCloudViewController,
            UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TutorialSettingsViewController") as! TutorialSettingsViewController,
            UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TutorialDisconnectViewController") as! TutorialDisconnectViewController,
            ] 
    }()
    
    var pageControl = UIPageControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dataSource = self
        self.delegate = self
        
        //Set the view controllers with forward movement and animation, this way we will achieve a nice animation like moving pages of a book when swiping the finger forward and backward
        setViewControllers([subViewControllers[0]], direction: .forward, animated: true, completion: nil)
        
        configurePageControl()
    }
    
    //Load the view controller once the transition between views (animated transition) has been completed
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let pageContentViewController = pageViewController.viewControllers![0]
        self.pageControl.currentPage = subViewControllers.index(of: pageContentViewController)!
    }
    
    //Loads the previous subcontroller of the array
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController?{
        let currentIndex:Int = subViewControllers.index(of: viewController) ?? 0
        if(currentIndex <= 0){
            return nil
        }
        return subViewControllers[currentIndex-1]
    }
    
    //Loads the next subcontroller of the array
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController?{
        let currentIndex:Int = subViewControllers.index(of: viewController) ?? 0
        if(currentIndex >= subViewControllers.count-1){
            return nil
        }
        return subViewControllers[currentIndex+1]
    }
    
    //Configure the carousel of images, starting on the first subcontroller.
    func configurePageControl(){
        pageControl = UIPageControl(frame: CGRect(x: 0, y: UIScreen.main.bounds.maxY - 50, width: UIScreen.main.bounds.width, height: 50))
        pageControl.numberOfPages = subViewControllers.count
        pageControl.currentPage = 0
        pageControl.tintColor = UIColor.black
        pageControl.pageIndicatorTintColor = UIColor.white
        pageControl.currentPageIndicatorTintColor = UIColor.black
        self.view.addSubview(pageControl)
    }
}
