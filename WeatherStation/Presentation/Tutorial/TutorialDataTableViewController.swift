//
//  TutorialDataTableViewController.swift
//  WeatherStationDemo
//
//  Created by MKMac1 on 24/10/2018.
//  Copyright © 2018 MK. All rights reserved.
//

import UIKit

//This view controller includes in the storyboard the image with the corresponding screenshot and the text with the explanation is written once the view is loaded.
class TutorialDataTableViewController: UIViewController{
    @IBOutlet weak var explanationLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        explanationLabel.text = "You can choose the parameter to explore. The last 4 measurements will be displayed."
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
