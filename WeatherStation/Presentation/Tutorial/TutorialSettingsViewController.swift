//
//  TutorialViewControllerSettings.swift
//  WeatherStationDemo
//
//  Created by MKMac1 on 24/10/2018.
//  Copyright © 2018 MK. All rights reserved.
//

import UIKit

//This view controller includes in the storyboard the image with the corresponding screenshot and the text with the explanation is written once the view is loaded.
class TutorialSettingsViewController: UIViewController{
    @IBOutlet weak var explanationLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        explanationLabel.text = "Units and refreshing rate can be configured in the Settings menu."
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
