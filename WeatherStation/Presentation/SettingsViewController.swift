//
//  SettingsScreenController.swift
//  WeatherStationDemo
//
//  Created by MKMac1 on 27/09/2018.
//  Copyright © 2018 MK. All rights reserved.
//

import Foundation
import CoreBluetooth
import UIKit

//Settings view controller shows the Application version, allows to change the data refresh rate from 1 sec to 1 min and allows to change the temperature degrees from ºC to ºF.
class SettingsViewController: UIViewController, BLEDelegate {
    
    @IBOutlet weak var deviceMACAddress: UILabel!
    @IBOutlet weak var appVersion: UILabel!
    @IBOutlet weak var switchRefreshRate: UISwitch!
    @IBOutlet weak var switchTempUnit: UISwitch!
    @IBOutlet weak var deviceMACAddressTitle: UILabel!
    @IBOutlet weak var rapidIoTPicture: UIImageView!
     
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Get the app version from the App configuration
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String{
            self.appVersion.text = version
        }
        
        BLEManager.sharedBLEInstance.delegate = self
        
        //If there is a rapid iot device connected, show the picture and device address of the device
        if(BLEManager.sharedBLEInstance.isConnected()){
            rapidIoTPicture.isHidden = false
            deviceMACAddress.isHidden = false
            deviceMACAddressTitle.isHidden = false
            
            deviceMACAddress?.text? = DataFormatting.reformatMACAddressWithColon(macAddress: Device.deviceInstance.getBleAddress()) ?? "Unknown"
        }
        else{
            rapidIoTPicture.isHidden = true
            deviceMACAddress.isHidden = true
            deviceMACAddressTitle.isHidden = true
        }
        
        let dataRefreshRate  = Persistent.readDataRefreshRate()
        if(dataRefreshRate == 1){
            switchRefreshRate.setOn(true, animated: true)
        }
        else{
            switchRefreshRate.setOn(false, animated: true)
        }
        
        
        let dataTempUnit = Persistent.readTemperatureUnit()
        if(dataTempUnit == Constants.TemperatureUnit.celsius.rawValue){
            switchTempUnit.setOn(true, animated: true)
        }
        else{
            switchTempUnit.setOn(false, animated: true)
        }
    }
    
    @IBAction func toggleRefreshRate(_ sender: UISwitch) {
        if(sender.isOn){
            if(!Persistent.writeDataRefreshRate(rateInSeconds: 1)){
                print("Could not write in persistent memory")
            }
        }
        else{
            if(!Persistent.writeDataRefreshRate(rateInSeconds: 60)){
                print("Could not write in persistent memory")
            }
        }
    }
    
    @IBAction func toggleTempUnit(_ sender: UISwitch) {
        if(sender.isOn){
            if(!Persistent.writeTemperatureUnit(unit: Constants.TemperatureUnit.celsius.rawValue)){
                print("Could not write in persistent memory")
            }
        }
        else{
            if(!Persistent.writeTemperatureUnit(unit: Constants.TemperatureUnit.fahrenheit.rawValue)){
                print("Could not write in persistent memory")
            }
        }
    }
    
    //BLEDelegate implementations
    func bleDidUpdateState(state: CBManagerState) {
        BLEManager.sharedBLEInstance.setBLEState(mState: state)
        if(state == .poweredOff){
            rapidIoTPicture.isHidden = true
            deviceMACAddress.isHidden = true
            deviceMACAddressTitle.isHidden = true
            
            let alert = UIAlertController(title: "Bluetooth error", message: "The bluetooth connection has stopped, please go back to the Welcome view and look for a device to connect to.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
        else if(state == .unsupported){
            rapidIoTPicture.isHidden = true
            deviceMACAddress.isHidden = true
            deviceMACAddressTitle.isHidden = true
            
            let alert = UIAlertController(title: "Bluetooth error", message: "The bluetooth adapter is unsupported in your mobile phone, unfortunately the application will not be able to connect to a Rapid IoT device in this phone", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
        else{
            rapidIoTPicture.isHidden = true
            deviceMACAddress.isHidden = true
            deviceMACAddressTitle.isHidden = true
            
            let alert = UIAlertController(title: "Bluetooth error", message: "The bluetooth adapter is in unknown state, try to reset it and enter in the application again", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func bleDidConnectToPeripheral(peripheral: CBPeripheral?) {
    }
    
    func bleDidDisconnectFromPeripheral() {
    }
    
    func bleDidReceiveData(char: String, data: Data?) {
        
    }
    
    func bleDidDiscoverDevice(peripheral: CBPeripheral?, advertisementLocalName: String?){
    }
    
    func bleDidFailConnectToPeripheral() {
    }
}
