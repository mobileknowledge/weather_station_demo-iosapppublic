//
//  ScanDevicesTableViewCell.swift
//  WeatherStationDemo
//
//  Created by MKMac1 on 22/10/2018.
//  Copyright © 2018 MK. All rights reserved.
//

import UIKit

//Cell definition to store the value of the Rapid IoT device name
class ScanDevicesTableViewCell: UITableViewCell{
    
    @IBOutlet weak var deviceLabel: UILabel!
}
