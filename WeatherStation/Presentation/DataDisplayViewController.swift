//
//  DataDisplayViewController.swift
//  WeatherStationDemo
//
//  Created by MKMac1 on 01/10/2018.
//  Copyright © 2018 MK. All rights reserved.
//

import UIKit
import CoreBluetooth

//The data display view contains the measurements table, which displays the last 4 measurements from the Rapid IoT sensors to the user. The cloud application settings can be accessed from this view, it will be possible to activate/deactive posting the data to the cloud and clear the current session.
class DataDisplayViewController: UIViewController, CloudSettingsViewDelegate, BLEDelegate {
    
    @IBOutlet weak var cloudSettingsButton: UIButton!
    @IBOutlet weak var deviceConnectedLabel: UILabel!
    @IBOutlet weak var sessionIDLabel: UILabel!
    
    //These boolean values will notify the read BLE data method when the app is ready to take another sample of the device. These variables are enabled according to the data refresh rate established in the settings menu.
    var isReadyToTakeTemperature: Bool = true
    var isReadyToTakeHumidity: Bool = true
    var isReadyToTakePressure: Bool = true
    var isReadyToTakeLight: Bool = true
    
    var networkSample: Network? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cloudSettingsButton.layer.cornerRadius = 15
        BLEManager.sharedBLEInstance.delegate = self
        deviceConnectedLabel.text = "Connected to: \(BLEManager.sharedBLEInstance.getCurrentPeripheralName() ?? "Unknown") "
        sessionIDLabel.text = "Session ID: \(Persistent.readSessionID() ?? "Unknown") "
        
        //Network instance that manages all the communications with the broker.
        networkSample = Network()
        networkSample?.setupBroker()
        networkSample?.connectToBroker()
        
        //Set four different timmers to control when the data read from the sensors should be stored in the app. Read the data refresh rate to set the timmer interval
        
        Timer.scheduledTimer(withTimeInterval: TimeInterval(Persistent.readDataRefreshRate() ?? 1), repeats: true){ timer in
            DispatchQueue.main.async {
                self.isReadyToTakeTemperature = true
            }
        }
        
        Timer.scheduledTimer(withTimeInterval: TimeInterval(Persistent.readDataRefreshRate() ?? 1), repeats: true){ timer in
            DispatchQueue.main.async {
                self.isReadyToTakeHumidity = true
            }
        }
        
        Timer.scheduledTimer(withTimeInterval: TimeInterval(Persistent.readDataRefreshRate() ?? 1), repeats: true){ timer in
            DispatchQueue.main.async {
                self.isReadyToTakePressure = true
            }
        }
        
        Timer.scheduledTimer(withTimeInterval: TimeInterval(Persistent.readDataRefreshRate() ?? 1), repeats: true){ timer in
            DispatchQueue.main.async {
                self.isReadyToTakeLight = true
            }
        }
        
        //Send measurements to the cloud application according to the data refresh rate defined in the settings view
        Timer.scheduledTimer(withTimeInterval: TimeInterval(Persistent.readDataRefreshRate() ?? 1), repeats: true){ timer in
            DispatchQueue.main.async {
                if(BLEManager.sharedBLEInstance.isConnected()){
                    if Persistent.readCloudPostSettings() ?? false{
                        if(DataValidation.isValidData(device: Device.deviceInstance)){
                            let getJSON = JSONGeneration.encodeMeasurementJSON(deviceObject: Device.deviceInstance)
                            self.networkSample?.sendDataToServer(data: getJSON, isDatabaseClean: false)
                        }
                    }
                    else{
                        print("Cloud post data is not active")
                    }
                }
            }
        }
    }
    
    //Present the Cloud settings as a Dialog.
    @IBAction func showCloudSettings(_ sender: UIButton) {
        let customAlert = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CloudSettingsViewController") as! CloudSettingsViewController

        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.cloudSettingsDelegate = self
        self.present(customAlert, animated: true, completion: nil)
    }

    //Callback implementation fo the clear cloud settings. If true sends a clear database command to the broker.
    func clearDatabaseTapped(clearDatabase: Bool) {
        if(clearDatabase){
            let clearDatabaseString = JSONGeneration.clearCloudDataJSON(sessionID: Persistent.readSessionID()!)
            self.networkSample?.sendDataToServer(data: clearDatabaseString!, isDatabaseClean: true)
        }
    }
    
    //BLEDelegate implementations
    func bleDidUpdateState(state: CBManagerState) {
        BLEManager.sharedBLEInstance.setBLEState(mState: state)
        if(state == .poweredOff){
            let alert = UIAlertController(title: "Bluetooth error", message: "The bluetooth connection has stopped, please go back to the Welcome view and look for a device to connect to.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
        else if(state == .unsupported){
            let alert = UIAlertController(title: "Bluetooth error", message: "The bluetooth adapter is unsupported in your mobile phone, unfortunately the application will not be able to connect to a Rapid IoT device in this phone", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
        else{            
            let alert = UIAlertController(title: "Bluetooth error", message: "The bluetooth adapter is in unknown state, try to reset it and enter in the application again", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func bleDidConnectToPeripheral(peripheral: CBPeripheral?) {
        networkSample?.pingServer()
    }
    
    func bleDidDisconnectFromPeripheral() {
        deviceConnectedLabel.text = "No device connected"
        let alert = UIAlertController(title: "Connection lost", message: "The connection with the device was lost. You will be redirected to the Scanning screen", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {action in
            self.navigationController?.popToRootViewController(animated: true)
        }))
        self.present(alert, animated: true)
    }
    
    func bleDidReceiveData(char: String, data: Data?) {
        if(char == Constants.TEMP_CHAR_READ_UUID){
            if(isReadyToTakeTemperature)
            {
                isReadyToTakeTemperature = false
                
                //Parse the value from the array and return temperature in Float value
                let hexEncoded = data!.hexEncodedString()
                let element1 = hexEncoded.substring(with: 4..<6)
                let element2 = hexEncoded.substring(with: 2..<4)
                let sum = element1 + element2
                
                let result = Float32(strtoul(sum,nil,16))/100
                
                //Get current timestamp and form measure object
                let now = UInt64(NSDate().timeIntervalSince1970)
                
                //Store the measure in the device object
                let newMeasurement = Measurement(value: result, timestamp: now)
                Device.deviceInstance.addTemperatureValue(measure: newMeasurement)
                
                TableUpdater.tableUpdaterInstance.updateTable(sensorType: Constants.SensorType.temperature)
            }
        }
        else if(char == Constants.HUMIDITY_CHAR_READ_UUID){
            if(isReadyToTakeHumidity)
            {
                isReadyToTakeHumidity = false
                
                let hexEncoded = data!.hexEncodedString()
                let element1 = hexEncoded.substring(with: 2..<4)
                let element2 = hexEncoded.substring(with: 0..<2)
                let sum = element1 + element2
                
                let result = Float32(strtoul(sum,nil,16))/100
                
                //Get current timestamp and form measure object
                let now = UInt64(NSDate().timeIntervalSince1970)
                
                //Store the measure in the device object
                let newMeasurement = Measurement(value: result, timestamp: now)
                Device.deviceInstance.addHumidityValue(measure: newMeasurement)
                
                TableUpdater.tableUpdaterInstance.updateTable(sensorType: Constants.SensorType.humidity)
            }
        }
        else if(char == Constants.PRESSURE_CHAR_READ_UUID){
            if(isReadyToTakePressure)
            {
                isReadyToTakePressure = false
                
                let hexEncoded = data!.hexEncodedString()
                let element1 = hexEncoded.substring(with: 2..<4)
                let element2 = hexEncoded.substring(with: 0..<2)
                let sum = element1 + element2
                
                let result = UInt16(strtoul(sum,nil,16))
                
                //Get current timestamp and form measure object
                let now = UInt64(NSDate().timeIntervalSince1970)
                
                //Store the measure in the device object
                let newMeasurement = Measurement(value: Float(result), timestamp: now)
                Device.deviceInstance.addPressureValue(measure: newMeasurement)
                
                TableUpdater.tableUpdaterInstance.updateTable(sensorType: Constants.SensorType.pressure)
            }
        }
        else if(char == Constants.LIGHT_CHAR_READ_UUID){
            if(isReadyToTakeLight)
            {
                isReadyToTakeLight = false
                
                let data = (data!).withUnsafeBytes{$0.pointee} as Float
                let valueWithTwoDecimals = Float(round(100*data)/100)
                
                //Get current timestamp and form measure object
                let now = UInt64(NSDate().timeIntervalSince1970)
                
                //Store the measure in the device object
                let newMeasurement = Measurement(value: valueWithTwoDecimals, timestamp: now)
                Device.deviceInstance.addLightValue(measure: newMeasurement)
                
                TableUpdater.tableUpdaterInstance.updateTable(sensorType: Constants.SensorType.light)
            }
        }
        else if(char == Constants.MAC_ADDRESS_CHAR_READ_UUID){
            
            //When the BLE MAC address is read we need to convert to EUI64 format and store in the Device object, which is shared throughout the app.
            let bleMACAddress = DataFormatting.convertMACAddressToEUI64(value: data!)
            Device.deviceInstance.setBleAddress(address: bleMACAddress)
        }
    }
    
    func bleDidDiscoverDevice(peripheral: CBPeripheral?, advertisementLocalName: String?){
    }
    
    func bleDidFailConnectToPeripheral() {
        print("Failed to connect to peripheral")
    }
}
