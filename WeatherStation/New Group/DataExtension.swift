//
//  DataExtension.swift
//  WeatherStationDemo
//
//  Created by MKMAC on 15/10/2018.
//  Copyright © 2018 MK. All rights reserved.
//

import Foundation

//Extend Data class to add hexadecimal strings which is used in some parts of the code (for instance in the session ID generation)
extension Data{
    
    struct HexEncodingOptions : OptionSet{
        let rawValue : Int
        static let upperCase = HexEncodingOptions(rawValue: 1 << 0)
    }

    func hexEncodedString(options: HexEncodingOptions = []) -> String{
        let hexDigits = Array((options.contains(.upperCase) ? "0123456789ABCDEF" : "0123456789abcdef").utf16)
        var chars: [unichar] = []
        for byte in self{
            chars.append(hexDigits[Int(byte/16)])
            chars.append(hexDigits[Int(byte % 16)])
        }
        return String(utf16CodeUnits: chars, count: chars.count)
    }
}

//Extend the String class in order to support some extra functionalities needed in the code (substring operations)
extension String {
    func index(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }
    
    func substring(from: Int) -> String {
        let fromIndex = index(from: from)
        return substring(from: fromIndex)
    }
    
    func substring(to: Int) -> String {
        let toIndex = index(from: to)
        return substring(to: toIndex)
    }
    
    func substring(with r: Range<Int>) -> String {
        let startIndex = index(from: r.lowerBound)
        let endIndex = index(from: r.upperBound)
        return substring(with: startIndex..<endIndex)
    }
}
