//
//  Constants.swift
//  WeatherStationDemo
//
//  Created by Luis Pascual on 11/10/2018.
//  Copyright © 2018 MK. All rights reserved.
//

import Foundation
class Constants : NSObject{
    static let WEATHER_SERVICE_UUID = "01FF5550-BA5E-F4EE-5CA1-EB1E5E4B1CE0"
    static let TEMP_CHAR_READ_UUID = "2A1C"
    static let HUMIDITY_CHAR_READ_UUID = "2A6F"
    static let LIGHT_CHAR_READ_UUID = "0AB5B672-C2CE-C4AB-E711-6CCBAA65C888"
    static let PRESSURE_CHAR_READ_UUID = "2A6D"
    static let MAC_ADDRESS_CHAR_READ_UUID = "0E1BB826-612E-ACA9-0E49-88F712EC1CB7"
    
    enum TemperatureUnit: String {
        case celsius = "Celsius"
        case fahrenheit = "Fahrenheit"
    }
    
    enum SensorType: String {
        case temperature = "temperature"
        case humidity = "humidity"
        case pressure = "pressure"
        case light = "light"
    }
}
