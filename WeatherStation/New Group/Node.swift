//
//  Node.swift
//  WeatherStationDemo
//
//  Created by MKMac1 on 30/10/2018.
//  Copyright © 2018 MK. All rights reserved.
//

import Foundation

// 1
public class Node<T> {
    // 2
    var value: T
    var next: Node<T>?
    weak var previous: Node<T>?
    
    // 3
    init(value: T) {
        self.value = value
    }
}
