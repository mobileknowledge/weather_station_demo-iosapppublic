//
//  Utilities.swift
//  WeatherStationDemo
//
//  Created by MKMac1 on 25/10/2018.
//  Copyright © 2018 MK. All rights reserved.
//

import Foundation

class Utilities: NSObject{

    //In case the data refresh rate is 60 seconds we need to compare the current timestamp with the timestamp of the last measure. When the comparison is less than 60 seconds we discard the data.
    static func discardDataDueToRefreshRate(sensorType : Constants.SensorType) -> Bool{
        if(Persistent.readDataRefreshRate() == 1){
            return false
        } else{ //Data refresh rate is 60 seconds, we need to discard some measurements
            let timestampLastMeasurement = Device.deviceInstance.getLastTimestamp(sensorType: sensorType)
            let currentTimestamp = Int(Date().timeIntervalSince1970)
            if((currentTimestamp - timestampLastMeasurement) > Persistent.readDataRefreshRate() ?? 1){ //Check the difference between the last measurement stored and the current one.
                return false
            }
            else{
                return true
            }
        }
    }
}
