//
//  TableUpdater.swift
//  WeatherStationDemo
//
//  Created by MKMac1 on 25/10/2018.
//  Copyright © 2018 MK. All rights reserved.
//

import Foundation

//Protocol that will be implemented in the Tables controller to know whent to update its corresponding table with the measurements updated.
protocol UpdateTableProtocol{
    func updateTable(sensorType: Constants.SensorType)
}

//This singleton class is shared between all the tables and each time a particular measurement is received the funcion updateTable of that corresponding measurement is updated.
class TableUpdater: NSObject{
    static let tableUpdaterInstance = TableUpdater()
    
    var updateTempTableDelegate: UpdateTableProtocol?
    var updateHumidityTableDelegate: UpdateTableProtocol?
    var updatePressureTableDelegate: UpdateTableProtocol?
    var updateLightTableDelegate: UpdateTableProtocol?
    
    func updateTable(sensorType: Constants.SensorType){
        switch sensorType {
        case Constants.SensorType.temperature:
            updateTempTableDelegate?.updateTable(sensorType: sensorType)
        case Constants.SensorType.humidity:
            updateHumidityTableDelegate?.updateTable(sensorType: sensorType)
        case Constants.SensorType.pressure:
            updatePressureTableDelegate?.updateTable(sensorType: sensorType)
        case Constants.SensorType.light:
            updateLightTableDelegate?.updateTable(sensorType: sensorType)
            
        }
    }
}
