//
//  Network.swift
//  WeatherStationDemo
//
//  Created by MKMac1 on 27/09/2018.
//  Copyright © 2018 MK. All rights reserved.
//

import UIKit
import CocoaMQTT

class Network: NSObject{
    var mqtt: CocoaMQTT?
    let messageBrokerURL = "b-a696cbdc-47a5-45be-a9ee-da0dd8f1d3b5-1.mq.us-west-2.amazonaws.com" //Amazon MQ endpoint to connect the application to the broker stored in AWS
    var isReadyToPublish : Bool
    
    //Checks if the mobile phone has internet capabilities.
    static func checkNetworkConnection() -> Bool{
        if Reachability.isConnectedToNetwork(){
            print("Internet Connection Available!")
            return true;
        }else{
            print("Internet Connection not Available!")
            return false;
        }
    }
    
    override init(){
        isReadyToPublish = false
    }
    
    //Create a client object in the Amazon MQ instance. This client will publish the data to a particular queue that will be redirected to the cloud application in order to store this data in the database
    func setupBroker(){
        let clientID = "CocoaMQTT-" + String(ProcessInfo().processIdentifier)
        mqtt = CocoaMQTT(clientID: clientID, host: messageBrokerURL, port: 8883)
        mqtt!.dispatchQueue = DispatchQueue.global(qos: .userInitiated)
        mqtt!.username = "admin"
        mqtt!.password = "admin1234567"
        mqtt!.willMessage = CocoaMQTTWill(topic: "/will", message: "dieout")
        mqtt!.keepAlive = 60
        mqtt!.cleanSession = true
        mqtt!.delegate = self
        mqtt!.enableSSL = true
    }
    
    //Connect to the broker we have configured in SetupBroker function.
    func connectToBroker(){
        if(mqtt!.connect()){
            print("Starting connection to MQTT broker")
        }
        else{
            print("Could not connect to the MQTT broker")
        }
    }
    
    //Disconnect from the broker.
    func disconnectFromBroker(){
        mqtt!.disconnect()
    }
    
    //Ping the Cloud application, this is necessary to activate the subscription of the queues.
    func pingServer(){
        let url = URL(string: "weatherstationcloudapp.us-west-2.elasticbeanstalk.com")
        
        _ = URLSession.shared.dataTask(with: url!){(data, response, error) in
            guard let data = data else { return }
            print(String(data: data, encoding: .utf8)!)
        }
    }
    
    //Send a JSON data to the server (in case that the cloud posting option is enabled). All the data will be sent to the server with QOS 2 (Send exactly once)
    func sendDataToServer(data: String, isDatabaseClean: Bool){
        if(isReadyToPublish){
            if(!isDatabaseClean){ //Check if the message sent belongs to a clean database command, in this case redirect to another queue in the broker.
                mqtt?.publish("weather-station-iOS", withString: data, qos: .qos2)
            }
            else{
                mqtt?.publish("weather-station-clear-database", withString: data, qos: .qos2)
            }
        }
    }
}

//Delegate functions for the CocoaMQTTDelegate object.
extension Network: CocoaMQTTDelegate {
    // Optional ssl CocoaMQTTDelegate
    func mqtt(_ mqtt: CocoaMQTT, didReceive trust: SecTrust, completionHandler: @escaping (Bool) -> Void) {
        TRACE("trust: \(trust)")
        completionHandler(true)
    }
    
    public func mqtt(_ mqtt: CocoaMQTT, didConnectAck ack: CocoaMQTTConnAck) {
        TRACE("ack: \(ack)")
        if ack == .accept {
            isReadyToPublish = true;
        }
    }
    
    public func mqtt(_ mqtt: CocoaMQTT, didStateChangeTo state: CocoaMQTTConnState) {
        TRACE("new state: \(state)")
    }
    
    public func mqtt(_ mqtt: CocoaMQTT, didPublishMessage message: CocoaMQTTMessage, id: UInt16) {
        TRACE("message: \(message.string?.description ?? "Unknown description"), id: \(id)")
    }
    
    public func mqtt(_ mqtt: CocoaMQTT, didPublishAck id: UInt16) {
        TRACE("id: \(id)")
    }
    
    public func mqtt(_ mqtt: CocoaMQTT, didReceiveMessage message: CocoaMQTTMessage, id: UInt16 ) {
        TRACE("message: \(message.string?.description ?? "unkonwn message"), id: \(id)")
    }
    
    public func mqtt(_ mqtt: CocoaMQTT, didSubscribeTopic topic: String) {
        TRACE("topic: \(topic)")
    }
    
    public func mqtt(_ mqtt: CocoaMQTT, didUnsubscribeTopic topic: String) {
        TRACE("topic: \(topic)")
    }
    
    public func mqttDidPing(_ mqtt: CocoaMQTT) {
        TRACE()
    }
    
    public func mqttDidReceivePong(_ mqtt: CocoaMQTT) {
        TRACE()
    }
    
    public func mqttDidDisconnect(_ mqtt: CocoaMQTT, withError err: Error?) {
        TRACE("\(err.debugDescription)")
    }
}

//Extend the object to add the TRACE functionality that will add a verbose log in the console of XCode.
extension Network {
    func TRACE(_ message: String = "", fun: String = #function) {
        let names = fun.components(separatedBy: ":")
        var prettyName: String
        if names.count == 1 {
            prettyName = names[0]
        } else {
            prettyName = names[1]
        }
        
        if fun == "mqttDidDisconnect(_:withError:)" {
            prettyName = "didDisconect"
        }
        
        print("[TRACE] [\(prettyName)]: \(message)")
    }
}

extension Optional {
    // Unwarp optional value for printing log only
    var description: String {
        if let warped = self {
            return "\(warped)"
        }
        return ""
    }
}
