//
//  BLE.swift
//  WeatherStationDemo
//
//  Created by MKMac1 on 27/09/2018.
//  Copyright © 2018 MK. All rights reserved.
//

import Foundation
import CoreBluetooth
import BlueCapKit

public enum AppError : Error {
    case dataCharacteristicNotFound
    case enabledCharacteristicNotFound
    case updateCharacteristicNotFound
    case serviceNotFound
    case invalidState
    case resetting
    case poweredOff
    case unknown
    case unlikely
}

class BLE{
    init(){
        let manager = CentralManager(options: [CBCentralManagerOptionRestoreIdentifierKey : "us.gnos.BlueCap.central-manager-documentation" as NSString])

        let stateChangeFuture = manager.whenStateChanges()
    
        let scanFuture = stateChangeFuture.flatMap { [weak manager] state -> FutureStream<Peripheral> in
            guard let manager = manager else {
                throw AppError.unlikely
            }
            switch state {
            case .poweredOn:
                return manager.startScanning(forServiceUUIDs: [serviceUUID])
            case .poweredOff:
                throw AppError.poweredOff
            case .unauthorized, .unsupported:
                throw AppError.invalidState
            case .resetting:
                throw AppError.resetting
            case .unknown:
                throw AppError.unknown
            }
        }

        scanFuture.onFailure { [weak manager] error in
            guard let appError = error as? AppError else {
                return
            }
            switch appError {
                case .invalidState:
                    break
                case .resetting:
                    manager?.reset()
                case .poweredOff:
                    break
                case .unknown:
                    break
            case .dataCharacteristicNotFound:
                <#code#>
            case .enabledCharacteristicNotFound:
                <#code#>
            case .updateCharacteristicNotFound:
                <#code#>
            case .serviceNotFound:
                <#code#>
            case .unlikely:
                <#code#>
            }
        }
    }
}
