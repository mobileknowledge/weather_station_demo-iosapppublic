//
//  BLEManager
//  WeatherStationDemo
//
//  Created by MKMac1 on 27/09/2018.
//  Copyright © 2018 MK. All rights reserved.
//

import Foundation
import CoreBluetooth
import NSData_FastHex

protocol BLEDelegate {
    func bleDidUpdateState(state: CBManagerState)
    func bleDidConnectToPeripheral(peripheral: CBPeripheral?)
    func bleDidFailConnectToPeripheral()
    func bleDidDisconnectFromPeripheral()
    func bleDidReceiveData(char: String, data: Data?)
    func bleDidDiscoverDevice(peripheral: CBPeripheral?, advertisementLocalName: String?)
}

class BLEManager: NSObject, CBCentralManagerDelegate, CBPeripheralDelegate {

    //Create object as singleton. This instance will be used throughout the whole application as there will be only one Bluetooth connection at a time.
    static let sharedBLEInstance = BLEManager()
    
    var delegate: BLEDelegate?
    
    private      var isScanning: Bool! = false
    private      var centralManager:   CBCentralManager?
    private      var bleState: CBManagerState = CBManagerState.unknown
    private      var activePeripheral: CBPeripheral?
    private      var activePeripheralName: String?
    private      var characteristics = [String : CBCharacteristic]()
    private(set) var peripherals     : [(CBPeripheral, String)] = []
    private      var RSSICompletionHandler: ((NSNumber?, NSError?) -> ())?
  
    private      var mBufferOffset: Int = 0
    private      var mExpectedSize: Int = 0
    private      var receivedData: NSMutableData?
    private      var semaphore:DispatchSemaphore?
    private      var semCanSendData:DispatchSemaphore?
    private      let WEARABLE_BUFFER_SIZE:Int = 20
    
    override init() {
        super.init()
        
        self.centralManager = CBCentralManager(delegate: self, queue: DispatchQueue.main, options: [CBCentralManagerOptionShowPowerAlertKey: false])
        semaphore = nil
        semCanSendData = DispatchSemaphore(value: 1)
    }
    
    //Check if the Bluetooth adapter is available (powered on)
    func isBLEAvailable() -> Bool{
        return self.centralManager!.state == .poweredOn
    }
    
    //Check if the Bluetooth is scanning for devices
    func isBLEScanning() -> Bool{
        return isScanning
    }
    
    //Start Bluetooth scan, in the scanning options we add the UUID for the Rapid IoT kits, this way we avoid receiving advertising messages from external devices
    func startScan() {
        if !isBLEAvailable() {
            print("[ERROR] Couldn´t start scanning, BLE not available")
            return
        }
        
        print("Start bluetooth scan")
        
        isScanning = true
        
        let services:[CBUUID] = [CBUUID(string: Constants.WEATHER_SERVICE_UUID)]
        self.centralManager!.scanForPeripherals(withServices: services, options: [CBCentralManagerScanOptionAllowDuplicatesKey: true])
    }
    
    //Stop Bluetooth scan
    func stopScan() {
        print("Stop bluetooth scan")
        
        self.centralManager!.stopScan()
        isScanning = false
    }
    
    //Connect to the bluetooth peripheral introduced as parameter
    func connectToPeripheral(peripheral: CBPeripheral) {
        if !isBLEAvailable() {
            print("[ERROR] Couldn´t connect to peripheral")
        }
        
        print("Connecting to peripheral: \(peripheral.identifier.uuidString)....")
        self.centralManager!.connect(peripheral, options: nil)
    }
    
    //Disconnect from the current peripheral
    func disconnectFromPeripheral() -> Bool {
        if !isBLEAvailable() {
            print("[ERROR] Couldn´t disconnect from peripheral")
            return false
        }
        
        if(activePeripheral != nil){
            print("Disconnecting from peripheral...")
            self.centralManager!.cancelPeripheralConnection(activePeripheral!)
        }
        
        return true
    }
    
    //Set state of the bluetooth adapter
    func setBLEState(mState: CBManagerState){
        self.bleState = mState
    }
    
    //Get state of the bluetooth adapter
    func getBLEState() -> CBManagerState{
        return self.bleState
    }
    
    //Read value of the MAC Address characteristic
    func readMACAddressCharacteristic() {
        guard let char = self.characteristics[Constants.MAC_ADDRESS_CHAR_READ_UUID] else { return }
        self.activePeripheral?.readValue(for: char)
    }

    //Enable bluetooth notify from temperature characteristic. It is mandatory to enable this characteristic to receive the value from this sensor.
    func enableNotificationsTemp(enable: Bool) {
        guard let char = self.characteristics[Constants.TEMP_CHAR_READ_UUID] else { return }
        self.activePeripheral?.setNotifyValue(enable, for: char)
    }

    //Enable bluetooth notify from humidity characteristic. It is mandatory to enable this characteristic to receive the value from this sensor.
    func enableNotificationsHumidity(enable: Bool) {
        guard let char = self.characteristics[Constants.HUMIDITY_CHAR_READ_UUID] else { return }
        self.activePeripheral?.setNotifyValue(enable, for: char)
    }
    
    //Enable bluetooth notify from pressure characteristic. It is mandatory to enable this characteristic to receive the value from this sensor.
    func enableNotificationsPressure(enable: Bool) {
        guard let char = self.characteristics[Constants.PRESSURE_CHAR_READ_UUID] else { return }
        self.activePeripheral?.setNotifyValue(enable, for: char)
    }
    
    //Enable bluetooth notify from light characteristic. It is mandatory to enable this characteristic to receive the value from this sensor.
    func enableNotificationsLight(enable: Bool) {
        guard let char = self.characteristics[Constants.LIGHT_CHAR_READ_UUID] else { return }
        self.activePeripheral?.setNotifyValue(enable, for: char)
    }
    
    //Read RSSI value from the current connected device.
    func readRSSI(completion: @escaping (_ RSSI: NSNumber?, _ error: NSError?) -> ()) {
        self.RSSICompletionHandler = completion
        self.activePeripheral!.readRSSI()
    }
    
    func getCurrentPeripheral() -> CBPeripheral? {
        if(isConnected()){
            return activePeripheral
        }
        else{
            return nil
        }
    }
    
    func getCurrentPeripheralName() -> String?{
        return activePeripheralName
    }
    
    func setCurrentPeripheralName(advName: String){
        activePeripheralName = advName
    }
    
    func isConnected() -> Bool {
        if activePeripheral == nil {
            return false
        }
        return true
    }
    
    //CBCentralManager delegate
    //This method gets called when a new device has been connected, it informs about the Bluetooth adapter status.
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        let mState:CBManagerState = central.state
        
        setBLEState(mState: mState)
        
        switch mState {
        case .unknown:
            print("Central manager state: Unknown")
            break
            
        case .resetting:
            print("Central manager state: Resseting")
            break
            
        case .unsupported:
            print("Central manager state: Unsupported")
            break
            
        case .unauthorized:
            print("Central manager state: Unauthorized")
            break
            
        case .poweredOff:
            print("Central manager state: Powered off")
            
            self.delegate?.bleDidDisconnectFromPeripheral()
            
            if(self.activePeripheral != nil){
                self.activePeripheral!.delegate = nil
                self.activePeripheral = nil
                self.activePeripheralName = nil
            }
            self.characteristics.removeAll(keepingCapacity: false)
            break
            
        case .poweredOn:
            print("Central manager state: Powered on")
            break
        }
        
        self.delegate?.bleDidUpdateState(state: mState)
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        print("New device discovered: \(peripheral.name ?? "Couldn't get device name..."), with adv. name \(advertisementData[CBAdvertisementDataLocalNameKey] ?? "Adversiting name not shown")")
        
        //Notify the delegate object that a new device has been discovered so that the UI can be changed accordingly
        self.delegate?.bleDidDiscoverDevice(peripheral: peripheral, advertisementLocalName: (advertisementData[CBAdvertisementDataLocalNameKey] as! String))
        
        //Add this new device to the peripheral array to keep track of the discovered devices
        peripherals.append((peripheral, advertisementData[CBAdvertisementDataLocalNameKey] as! String))
    }
    
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        print("[ERROR] Could not connect to peripheral \(peripheral.identifier.uuidString) error: \(error!.localizedDescription)")
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("Connected to peripheral \(peripheral.name ?? "Couldn't get device name")")
        
        //Notify the delegate object that the device has been connected so that the UI can be changed accordingly
        self.delegate?.bleDidConnectToPeripheral(peripheral: peripheral)
        
        self.activePeripheral = peripheral
        self.activePeripheral?.delegate = self
        self.activePeripheral?.discoverServices(nil)
        
        self.stopScan()
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        var text = "Disconnected from peripheral: \(peripheral.identifier.uuidString)"
        
        if error != nil {
            text += ". Error: \(error!.localizedDescription)"
            print(text)
        }
        
        //Notify the delegate object that the device has been disconnected so that the UI can be changed accordingly
        self.delegate?.bleDidDisconnectFromPeripheral()
        
        //Disable all the notifications from the sensors characteristics
        self.enableNotificationsTemp(enable: false)
        self.enableNotificationsHumidity(enable: false)
        self.enableNotificationsPressure(enable: false)
        self.enableNotificationsLight(enable: false)
        
        if(self.activePeripheral != nil){
            self.activePeripheral!.delegate = nil
            self.activePeripheral = nil
            self.activePeripheralName = nil
        }
        
        self.characteristics.removeAll(keepingCapacity: false) //Remove all the stored characteristics
        
    }
    
    //CBPeripheral delegate
    //When the services have been discovered we call the chracteristics of that service.
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        if error != nil {
            print("[ERROR] Error discovering services. \(error!.localizedDescription)")
            return
        }
        
        for service in peripheral.services! {
            peripheral.discoverCharacteristics(nil, for: service)
        }
    }
    
    //When the device services have been read we need to read all the chracteristics from the MAC address and sensors data
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        if error != nil {
            print("[ERROR] Error discovering characteristics. \(error!.localizedDescription)")
            return
        }
        
        for characteristic in service.characteristics! {
            self.characteristics[characteristic.uuid.uuidString] = characteristic
            if(characteristic.uuid.uuidString == Constants.MAC_ADDRESS_CHAR_READ_UUID){
                self.readMACAddressCharacteristic()
                self.enableNotificationsTemp(enable: true)
                self.enableNotificationsHumidity(enable: true)
                self.enableNotificationsPressure(enable: true)
                self.enableNotificationsLight(enable: true)
            }
        }
    }
    
    //Each time a new value is received from any of the characteristics this function gets called and we notify the delegate with the new data read.
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        if error != nil {
            print("[ERROR] Error updating value. \(error!.localizedDescription)")
            return
        }
        
        /* Data change */
        if characteristic.uuid.uuidString == Constants.TEMP_CHAR_READ_UUID {
            self.delegate?.bleDidReceiveData(char: Constants.TEMP_CHAR_READ_UUID, data: characteristic.value! as Data)
        }
        else if characteristic.uuid.uuidString == Constants.HUMIDITY_CHAR_READ_UUID {
            self.delegate?.bleDidReceiveData(char: Constants.HUMIDITY_CHAR_READ_UUID, data: characteristic.value! as Data)
        }
        else if characteristic.uuid.uuidString == Constants.PRESSURE_CHAR_READ_UUID {
            self.delegate?.bleDidReceiveData(char: Constants.PRESSURE_CHAR_READ_UUID, data: characteristic.value! as Data)
        }
        else if characteristic.uuid.uuidString == Constants.LIGHT_CHAR_READ_UUID {
            self.delegate?.bleDidReceiveData(char: Constants.LIGHT_CHAR_READ_UUID, data: characteristic.value! as Data)
        }
        else if characteristic.uuid.uuidString == Constants.MAC_ADDRESS_CHAR_READ_UUID {
            self.delegate?.bleDidReceiveData(char: Constants.MAC_ADDRESS_CHAR_READ_UUID, data: characteristic.value! as Data)
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {
    }
    
    func peripheral(_ peripheral: CBPeripheral, didReadRSSI RSSI: NSNumber, error: Error?) {
        self.RSSICompletionHandler?(RSSI, error! as NSError)
        self.RSSICompletionHandler = nil
    }

    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        semCanSendData?.signal()
    }

}
