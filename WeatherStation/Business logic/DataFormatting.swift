//
//  DataFormatting.swift
//  WeatherStationDemo
//
//  Created by MKMac1 on 27/09/2018.
//  Copyright © 2018 MK. All rights reserved.
//

import Foundation

class DataFormatting : NSObject{
    //Convert the float value introduced as parameter to fahrenheit degrees.
    static func convertCelsiusToFahrenheit(value: Float) -> Float{
        let celsius = value
        let fahrenheit = (celsius * (9/5)) + 32
        
        return fahrenheit
    }
    
    //The MAC address is sent in EUI-48 format, it is necessary to perform some operations to the value read to display in the well-known EUI-64 format
    //Example: Value read: B48A12376000 is converted to 006037 0000 128AB4
    static func convertMACAddressToEUI64(value: Data?) -> String{
        var result:[UInt8] = []
        var finalMACString = String()
        var i = 5, j = 0
        
        while (i > 2){
            result.append(value![i])
            j += 1
            i -= 1
        }
        
        result.append(0)
        result.append(0)
        
        i = 2
        j = 6
        while (i > -1){
            result.append(value![i])
            j += 1
            i -= 1
        }
        
        for index in 0..<8 {
            finalMACString.append(String(format: "%02X", result[index]))
        }
        
        return finalMACString
    }
    
    //Add a colon each 4 digits in the MAC address
    static func reformatMACAddressWithColon(macAddress: String?) -> String?{
        var finalMACAddress: String = ""
        if macAddress == "Unknown"{
            return nil
        }
        
        for i in stride(from: 0, to: 15, by: 2){
            finalMACAddress.append(macAddress!.substring(with: i..<(i+2)))
            if(i<14){
                finalMACAddress.append(":")
            }
        }
        return finalMACAddress
    }
}
