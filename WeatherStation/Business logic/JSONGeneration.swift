//
//  JSONGeneration.swift
//  WeatherStationDemo
//
//  Created by MKMac1 on 27/09/2018.
//  Copyright © 2018 MK. All rights reserved.
//

import Foundation

//Created a Device structure to be used in the JSON messages sent to the cloud application.
//Example of a JSON structure:
// {
// "bleMACAddress":"0060370000127496",

// "sessionID":"75C31C",
// "measurement":{"humidity":20.190000534057617,"temperature":29.360000610351562,"light":11.159999847412109,"timestamp":1544544113,"pressure":1017},
// "name":"RPK-7496"
// }
struct DeviceJSON {
    var name: String
    var bleMACAddress: String
    var sessionID: String
    var temperature: Float
    var humidity: Float
    var pressure: Float
    var light: Float
    var timestamp: UInt64
    // Coding Keys
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case bleMACAddress = "bleMACAddress"
        case sessionID = "sessionID"
        case measurement = "measurement"
        case temperature = "temperature"
        case humidity = "humidity"
        case pressure = "pressure"
        case light = "light"
        case timestamp = "timestamp"
    }
}

//{
// "SessionID":"0DFE17"
//}
struct ClearCloud {
    var SessionID: String
    // Coding Keys
    enum CodingKeys: String, CodingKey {
        case SessionID = "SessionID"
    }
}

class JSONGeneration: NSObject{
    //Create a JSON object with the measurements, in order to form the JSON object we use the DeviceJSON structure defined above. Use JSONEncoder functionality included in Swift.
    static func encodeMeasurementJSON(deviceObject: Device) -> String{
        let createStructure = DeviceJSON(name: deviceObject.getName(), bleMACAddress: deviceObject.getBleAddress(), sessionID: Persistent.readSessionID()!,
                                         temperature: deviceObject.getTemperature(position: 0).value!, humidity: deviceObject.getHumidity(position: 0).value!,
                                         pressure: deviceObject.getPressure(position: 0).value!, light: deviceObject.getLight(position: 0).value!,
                                         timestamp: deviceObject.getTemperature(position: 0).timestamp!)
        
        let jsonEncoder = JSONEncoder()
        let jsonData = try! jsonEncoder.encode(createStructure)
        return String(data: jsonData, encoding: String.Encoding.utf8) ?? "Unknown JSON"
    }

    //This function creates the JSON object sent to the cloud for the clear database functionality. In this case we don't use a predefined structure since the data to be included in the JSON object is quite simple.
    static func clearCloudDataJSON(sessionID: String) -> String?{
        let createStructure = ClearCloud(SessionID: sessionID)
        
        let jsonEncoder = JSONEncoder()
        let jsonData = try! jsonEncoder.encode(createStructure)
        
        print(String(data: jsonData, encoding: String.Encoding.utf8))
        return String(data: jsonData, encoding: String.Encoding.utf8) ?? "Unknown JSON"
        
    }
    
}

//Extend the Device JSON structure to add the desired definition for the JSON, i.e. the MAC address, session ID and Name will be in the main bracket of the JSON object and the measurements and timestamp will be grouped.
extension DeviceJSON: Encodable {
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(name, forKey: .name)
        try container.encode(bleMACAddress, forKey: .bleMACAddress)
        try container.encode(sessionID, forKey: .sessionID)
        var measurement = container.nestedContainer(keyedBy: CodingKeys.self, forKey: .measurement)
        try measurement.encode(temperature, forKey: .temperature)
        try measurement.encode(humidity, forKey: .humidity)
        try measurement.encode(pressure, forKey: .pressure)
        try measurement.encode(light, forKey: .light)
        try measurement.encode(timestamp, forKey: .timestamp)
    }
}

extension ClearCloud: Encodable {
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(SessionID, forKey: .SessionID)
    }
}
