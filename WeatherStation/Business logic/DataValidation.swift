//
//  DataValidation.swift
//  WeatherStationDemo
//
//  Created by MKMac1 on 27/09/2018.
//  Copyright © 2018 MK. All rights reserved.
//

import Foundation

class DataValidation : NSObject{
    //Check if the device object has at least one measurement stored.
    static func isValidData(device: Device) -> Bool{
        if(device.getTemperature(position: 0).value != nil
            && device.getHumidity(position: 0).value != nil
            && device.getPressure(position: 0).value != nil
            && device.getLight(position: 0).value != nil)
        {
            return true
        }
        else{
            return false
        }
    }
}
