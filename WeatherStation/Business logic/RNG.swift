//
//  RNG.swift
//  WeatherStationDemo
//
//  Created by MKMac1 on 27/09/2018.
//  Copyright © 2018 MK. All rights reserved.
//

import Foundation
class RNG: NSObject{
    
    //Generate random number with the number of digits introduced as parameter. The random number will contain hexadecimal characters.
    static func generateRandomNumber(digits: Int) -> String{
        let charactersSet = "ABCDEF1234567890"
        var generatedNumber = ""
        
        for _ in 0..<digits
        {
            let singleDigit = Int(arc4random_uniform(UInt32(charactersSet.count)))
            generatedNumber += String(charactersSet[charactersSet.index(charactersSet.startIndex, offsetBy: singleDigit)])
        }
        
        return generatedNumber
    }
}
