//
//  Device.swift
//  WeatherStationDemo
//
//  Created by MKMac1 on 27/09/2018.
//  Copyright © 2018 MK. All rights reserved.
//

import Foundation

class Device : NSObject{
    private var name: String?
    private var bleMACAddress: String?
    private var temperatureData : Array<Measurement> = Array<Measurement>(repeating: Measurement(value: nil,timestamp: nil), count: 4)
    private var pressureData : Array<Measurement> = Array<Measurement>(repeating: Measurement(value: nil,timestamp: nil), count: 4)
    private var humidityData : Array<Measurement> = Array<Measurement>(repeating: Measurement(value: nil,timestamp: nil), count: 4)
    private var lightData : Array<Measurement> = Array<Measurement>(repeating: Measurement(value: nil,timestamp: nil), count: 4)
    
    //The device object follows the singleton pattern, we will access this instance throughout the whole application.
    static let deviceInstance = Device()
    
    func setName(name: String){
        self.name = name
    }
    
    func setBleAddress(address: String){
        self.bleMACAddress = address
    }
    
    func getName() -> String{
        return self.name ?? "Unknown"
    }
    
    func getBleAddress() -> String{
        return self.bleMACAddress ?? "Unknown"
    }
    
    //The last measurement is added at the first element of the array, the previous measurements are swapped in the remaining positions of the array and the last measurement disappears.
    func addTemperatureValue(measure : Measurement){
        temperatureData.swapAt(2, 3)
        temperatureData.swapAt(1, 2)
        temperatureData.swapAt(0, 1)
        temperatureData[0] = measure
    }
    
    func getAllTemperatures() -> [Measurement]{
        return temperatureData
    }
    
    func getTemperature(position: Int) -> Measurement{
        return temperatureData[position]
    }
    
    //The last measurement is added at the first element of the array, the previous measurements are swapped in the remaining positions of the array and the last measurement disappears.
    func addHumidityValue(measure : Measurement){
        humidityData.swapAt(2, 3)
        humidityData.swapAt(1, 2)
        humidityData.swapAt(0, 1)
        humidityData[0] = measure
    }
    
    func getAllHumidities() -> [Measurement]{
        return humidityData
    }
    
    func getHumidity(position: Int) -> Measurement{
        return humidityData[position]
    }
    
    //The last measurement is added at the first element of the array, the previous measurements are swapped in the remaining positions of the array and the last measurement disappears.
    func addPressureValue(measure : Measurement){
        pressureData.swapAt(2, 3)
        pressureData.swapAt(1, 2)
        pressureData.swapAt(0, 1)
        pressureData[0] = measure
    }
    
    func getAllPressures() -> [Measurement]{
        return pressureData
    }
    
    func getPressure(position: Int) -> Measurement{
        return pressureData[position]
    }
    
    //The last measurement is added at the first element of the array, the previous measurements are swapped in the remaining positions of the array and the last measurement disappears.
    func addLightValue(measure : Measurement){
        lightData.swapAt(2, 3)
        lightData.swapAt(1, 2)
        lightData.swapAt(0, 1)
        lightData[0] = measure
    }
    
    func getAllLights() -> [Measurement]{
        return lightData
    }
    
    func getLight(position: Int) -> Measurement{
        return lightData[position]
    }
    
    //Get last timestamp function is needed to check if the new measurement fits the refresh rate that the user has configured (in case it is more than 60 seconds some measurements are skipped since we don't need them)
    func getLastTimestamp(sensorType: Constants.SensorType) -> Int{
        switch sensorType {
        case Constants.SensorType.temperature:
            if(temperatureData[0].value != nil){
                return Int(temperatureData[0].timestamp!)
            }
            else{
                return 0
            }
        case Constants.SensorType.humidity:
            if(humidityData[0].value != nil){
                return Int(humidityData[0].timestamp!)
            }
            else{
                return 0
            }
        case Constants.SensorType.pressure:
            if(pressureData[0].value != nil){
                return Int(pressureData[0].timestamp!)
            }
            else{
                return 0
            }
        case Constants.SensorType.light:
            if(lightData[0].value != nil){
                return Int(lightData[0].timestamp!)
            }
            else{
                return 0
            }
        }
    }
}
