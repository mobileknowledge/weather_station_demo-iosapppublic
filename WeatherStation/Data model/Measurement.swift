//
//  File.swift
//  WeatherStationDemo
//
//  Created by MKMac1 on 04/10/2018.
//  Copyright © 2018 MK. All rights reserved.
//

import Foundation

struct Measurement {
    var value: Float? //Stored in format: xx.xx
    var timestamp: UInt64? //Stored in format: UNIX timestampNS
}
